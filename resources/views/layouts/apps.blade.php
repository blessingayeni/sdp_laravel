<!DOCTYPE HTML>
<html lang="en">
    <head>
       <meta charset="utf-32">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Task</title>

        <!------Stylesheets starts--->
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="styler.css" rel="stylesheet" type="text/css">
        <!------Stylesheets Ends--->

    </head>
    <body>

        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="{{ url('/') }}">
                            Task List
                        </a>
                    </div>
                </div>

            </nav>
        </div>

        @yield('content')


    <!------Javascript starts--->
    <script src="js/bootstrap.js"></script>
    <script src="scripter.js"></script>
    <!------Javascript Ends--->
    </body>

</html>