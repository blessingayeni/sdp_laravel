@extends('layouts.apps')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Task
                </div>
                <div class="panel-body">
                    <!---Display Validation Error----->
                    @include('common.errors')

                    <!--Add New Task---->
                    <form action="{{ url('/task') }}" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                        <div class="form-group">
                            <label for="task" class="col-sm-3 control-label" title="">Task</label>
                            <div class="col-sm-6">
                                <input title="" type="text" name="name" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                    <button type="submit" class="btn btn-default">
                                        <span class="glyphicon glyphicon-plus"></span> Add Task

                                    </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>

            @if(count($tasks) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Current Task
                    </div>

                    <div class="panel-body">

                        <table class="table table-striped task-table">
                            <thead>
                                Task
                            </thead>
                            <thead>
                            &nbsp;
                            </thead>

                            <tbody>
                                @foreach($tasks as $task)
                                    <tr>
                                        <td class="table-text">
                                           <div>{{ $task->name }}</div>
                                        </td>
                                    

                                        <td >
                                            <form action="{{ url('task/'.$task->id) }}" method="POST">
                                                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn btn-danger">
                                                        <span class="glyphicon glyphicon-trash"></span> Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>

                </div>
            @endif
        </div>

    </div>

@stop